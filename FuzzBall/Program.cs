﻿using FuzzBall.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzBall
{
    class Program
    {
        static void Main(string[] args)
        {
            FuzzBallLogic logic = new FuzzBallLogic();
            for (int i = 0; i < 100; i++)
            {
                var result = logic.PlayFuzzBall(i);
                Console.WriteLine(result);
            }
            Console.ReadKey();
        }
    }
}
