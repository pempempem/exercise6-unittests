﻿using FuzzBall.Logic;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzBall.UnitTests
{
    [TestFixture]
    class FuzzBallLogicTests
    {
        public FuzzBallLogic Logic;

        [SetUp]
        public void Init()
        {
            Logic = new FuzzBallLogic();
        }

        //Tests for specific integers from 0 to 19
        #region Homework
        [Test]
        public void PlayFuzzBall_ValidOutputFor1_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(1);

            Assert.That(result, Is.EqualTo("1"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor2_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(2);

            Assert.That(result, Is.EqualTo("2"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor3_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(3);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor4_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(2);

            Assert.That(result, Is.EqualTo("2"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor5_ReturnStringBall()
        {
            var result = Logic.PlayFuzzBall(5);

            Assert.That(result, Is.EqualTo("Ball"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor6_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(6);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor7_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(7);

            Assert.That(result, Is.EqualTo("7"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor8_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(8);

            Assert.That(result, Is.EqualTo("8"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor9_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(9);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor10_ReturnStringBall()
        {
            var result = Logic.PlayFuzzBall(10);

            Assert.That(result, Is.EqualTo("Ball"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor11_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(11);

            Assert.That(result, Is.EqualTo("11"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor12_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(12);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor13_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(13);

            Assert.That(result, Is.EqualTo("13"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor14_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(14);

            Assert.That(result, Is.EqualTo("14"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor15_ReturnStringFuzzBall()
        {
            var result = Logic.PlayFuzzBall(15);

            Assert.That(result, Is.EqualTo("FuzzBall"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor16_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(16);

            Assert.That(result, Is.EqualTo("16"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor17_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(17);

            Assert.That(result, Is.EqualTo("17"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor18_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(18);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_ValidOutputFor19_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(19);

            Assert.That(result, Is.EqualTo("19"));
        }
        #endregion

        //Tests suitable for all integers from 0 to 19
        #region Normal Tests
        [Test]
        public void PlayFuzzBall_InputIsNotDivisibleBy3Or5_ReturnNumberAsString()
        {
            var result = Logic.PlayFuzzBall(4);

            Assert.That(result, Is.EqualTo("4"));
        }

        [Test]
        public void PlayFuzzBall_InputIsDivisibleBy3Only_ReturnStringFuzz()
        {
            var result = Logic.PlayFuzzBall(3);

            Assert.That(result, Is.EqualTo("Fuzz"));
        }

        [Test]
        public void PlayFuzzBall_InputIsDivisibleBy5Only_ReturnStringBall()
        {
            var result = Logic.PlayFuzzBall(5);

            Assert.That(result, Is.EqualTo("Ball"));
        }

        [Test]
        public void PlayFuzzBall_InputIsDivisibleBy3And5_ReturnStringFuzzBall()
        {
            var result = Logic.PlayFuzzBall(15);

            Assert.That(result, Is.EqualTo("FuzzBall"));
        }

        #endregion
    }
}
