﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzBall.Logic
{
    public class FuzzBallLogic
    {
        /// <summary>
        /// This method operates a FuzzBall game logic,it checks if parameter is divisible by 3, 5 or 3 and 5 at the same time  
        /// </summary>
        /// <param name="number">A given number</param>
        /// <returns>"Fuzz","Ball","FuzzBall" or a given number as a String</returns>
        public string PlayFuzzBall(int number)
        {
            if ((number % 3 == 0) && (number % 5 == 0))
                return "FuzzBall";

            if (number % 3 == 0)
                return "Fuzz";

            if (number % 5 == 0)
                return "Ball";

            return number.ToString();
        }
    }
}
